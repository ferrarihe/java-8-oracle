# java-11-oracle

Descargar la version de java11 utilizada para desarrollo


https://adoptopenjdk.net/installation.html#x64_linux-jdk


tar xzf OpenJDK11U-jdk_x64_linux_hotspot_11.0.13_8.tar.gz

export PATH=$PWD/jdk-11.0.13+8/bin:$PATH

java -version








# java-8-oracle

Descargar la version de java8 utilizada para desarrollo

Copiar a carpeta /opt:


`  sudo cp jdk-8u181-linux-x64.tar.gz /opt/`



Descomprimir java:


`
    cd /opt/
    sudo tar -xzvf jdk-8u181-linux-x64.tar.gz
`

Seteo de variables de entorno
Configura dentro de bashrc las variables de entorno de java (este file se ubica dentro de tu home :


```
cd --
nano .bashrc
 
# y al editar al final agrega estas lineas
 
export JAVA_HOME=/opt/jdk-xx  #reemplaza con el nombre de la carpeta que se creo en /opt/ en el paso anterior cuando se descomprimio
export PATH=$JAVA_HOME/bin:$PATH
```


Revisar que quedo correctamente instalado Java8
Cerrar la consola y abrir una nueva (para que tomen las variables de entorno recien creadas)


`java -version`


Ejecutar el comando anterior y deberia decir la versión de java instalada. Algo como lo siguiente

```
java version "1.8.0_171"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
```




